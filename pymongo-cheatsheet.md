### Setup
Linux Shell Commands
___

Start MongoDB
```
sudo service mongod start
```

Stop MongoDB
```
sudo service mongod stop
```

Restart MongoDB
```
sudo service mongod restart
```

Begin using MongoDB
```
mongo --host 127.0.0.1:27017
```

### Creating

Setup client
```python
dbclient = MongoClient('mongodb://localhost:27017/')
```

Create the database - Note: created lazily, i.e. only gets made once something is put into it
```python
db = dbclient['name_of_database']
```

Create a collection (SQL table 'equivalent')
```python
collection = db['collection_name']
```

### Add/Remove Data

Insert One Item
```python
collection.insert_one(data)
```

Insert Many Items
```python
collection.insert_many(data)
```

Replace One Item
```python
collection.replace_one(replace_this, with_this)
```

Update One Item
```python
db.collection.update_one(update_this, with_this)
```

Update Many Items
```python
db.collection.update_many(update_this, with_this)
```

Delete One Item
```python
db.collection.delete_one(data)
```

Delete Many Items
```python
db.collection.delete_many(data)
```

Remove Collection from database
```python
db.drop_collection('collection_name')
```

### Queries

Find one
```python
collection.find_one(key_pair)
```

Find All
```python
collection.find()
```

Limit results
```
collection.find().limit(limit)
```

Count of results
```python
collection.find().count()
```

Get list of all collection names in database
```python
db.list_collection_names()
```

Sorted results
```python
for doc in collection.find().sort('field', pymongo.ASCENDING):
    print(doc)
```